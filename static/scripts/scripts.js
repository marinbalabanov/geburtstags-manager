moment.locale('de-at');

function geburtstageAuflisten() {

    $.ajax({
        url: '/birthdays/all',
        success: function( response ) {
            var geburtstage = jQuery.parseJSON(response);
            $('#geburtstagstabelle').empty();
            for(var eintraege = 0; eintraege < geburtstage.birthdays.length; eintraege++) {
                var geloescht = "";
                if(geburtstage.birthdays[eintraege].birthdayDeleted === "true"){
                    geloescht = "Ja";
                } else if(geburtstage.birthdays[eintraege].birthdayDeleted === "false") {
                    geloescht = "Nein";
                } else {
                    geloescht = "Speicherfehler";
                };
                konvertiertesDatum = moment(geburtstage.birthdays[eintraege].birthdayYear + '-' + geburtstage.birthdays[eintraege].birthdayMonth + '-' + geburtstage.birthdays[eintraege].birthdayDay).format('LL');
                $('#geburtstagstabelle').append(`
                    <tr>
                        <th>${geburtstage.birthdays[eintraege].id}</th>
                        <td>${geburtstage.birthdays[eintraege].birthdayName}</td>
                        <td>${konvertiertesDatum}</td>
                        <td>${geloescht}</td>
                        <td>
                            <button type="button" data-editid="${geburtstage.birthdays[eintraege].id}" class="btn btn-primary btn-sm mr-2" data-toggle="modal" data-target="#bearbeitenModal">Bearbeiten</button>
                            <button type="button" data-deleteid="${geburtstage.birthdays[eintraege].id}" class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#loeschenModal">Löschen</button>
                        </td>
                    </tr>
                `);
            }
        }
    });

};

function geburtstagHinzufuegen( geburtstagsdaten ) {
    geburtstagsdaten.preventDefault();

    var geburtstagsName = $('#birthdayName').val();
    var geburtstagsTag = $('#birthdayDateDay').val();
    var geburtstagsMonat = $('#birthdayDateMonth').val();
    var geburtstagsJahr = $('#birthdayDateYear').val();

    $.ajax({
        url:'/birthdays/add',
        method:'post',
        data:{
            birthdayName : geburtstagsName,
            birthdayDay : geburtstagsTag,
            birthdayMonth : geburtstagsMonat,
            birthdayYear : geburtstagsJahr
        },
        success:function( successResponse ) {
            $('.toast').toast('show');
            $('#birthdayName').val('');
            $('#birthdayDateDay').val('01').change();
            $('#birthdayDateMonth').val('01').change();
            $('#birthdayDateYear').val(1980);
        }
    });

}

function einzelnenGeburtstag( geburtstagsid ) {
    geburtstagsid.preventDefault();

    var einzelnerGeburtstag = $('#birthdayId').val();

    $.ajax({
        url:'/birthdays/id',
        method:'post',
        data:{
            singleid : einzelnerGeburtstag
        },
        success:function( successResponse ) {

        }
    });

}

function geburtstagGeloeschtMarkieren() {
    var loeschId = parseInt($(this).attr('data-deleteid'));

    $.ajax({
        url:'/birthdays/delete',
        method:'delete',
        data:{
            deleteid : loeschId
        },
        success:function( successResponse ) {
            $('.toast').toast('show');
            geburtstageAuflisten();
        }
    });
};

function bearbeitetenGeburtstagSpeichern() {
    var geburtstagsName = $('#bearbeitenBirthdayName').val();
    var geburtstagsTag = $('#bearbeitenBirthdayDateDay').val();
    var geburtstagsMonat = $('#bearbeitenBirthdayDateMonth').val();
    var geburtstagsJahr = $('#bearbeitenBirthdayDateYear').val();
    var geburtstagsDeleted = $('#bearbeitenBirthdayDeleted').val();
    var bearbeitungsId = parseInt($(this).attr('data-editid'));

    $.ajax({
        url:'/birthdays/edit',
        method:'put',
        data:{
            editid : bearbeitungsId,
            editBirthdayName : geburtstagsName,
            editBirthdayDay : geburtstagsTag,
            editBirthdayMonth : geburtstagsMonat,
            editBirthdayYear : geburtstagsJahr,
            editBirthdayDeleted : geburtstagsDeleted
        },
        success:function( successResponse ) {
            $('.toast').toast('show');
            geburtstageAuflisten();
        }
    });
};

$( '#edit-tab' ).on( 'click', geburtstageAuflisten);
$( '#neuerGeburtstag' ).on( 'submit', geburtstagHinzufuegen);
$( '#loeschenButton' ).on( 'click', geburtstagGeloeschtMarkieren);
$( '#bearbeitenButton' ).on( 'click', bearbeitetenGeburtstagSpeichern);

$('#loeschenModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var markdeleted = button.data('deleteid');

    $.ajax({
        url:'/birthdays/id',
        method:'post',
        data:{
            singleid : markdeleted
        },
        success:function( response ) {
            var loescheintrag = response;
            var lesbaresDatum = moment(loescheintrag.birthdayYear + '-' + loescheintrag.birthdayMonth + '-' + loescheintrag.birthdayDay).format('LL');
            $('#loeschEintrag').empty();
            $('#loeschEintrag').append(`
                <th>${loescheintrag.id}</th>
                <td>${loescheintrag.birthdayName}</td>
                <td>${lesbaresDatum}</td>
            `);
            $('#loeschenButton').attr("data-deleteid", markdeleted);
        }
    });

    var modal = $(this);
    modal.find('.modal-title').text('Eintrag Nr. ' + markdeleted + ' löschen ' );
});

$('#bearbeitenModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var editId = button.data('editid');

    $.ajax({
        url:'/birthdays/id',
        method:'post',
        data:{
            singleid : editId
        },
        success:function( response ) {
            var eintragBearbeiten = response; 
            $('#bearbeitenBirthdayName').val(eintragBearbeiten.birthdayName);
            $('#bearbeitenBirthdayDateDay').val(String(eintragBearbeiten.birthdayDay)).change();
            $('#bearbeitenBirthdayDateMonth').val(String(eintragBearbeiten.birthdayMonth)).change();
            $('#bearbeitenBirthdayDateYear').val(eintragBearbeiten.birthdayYear);
            $('#bearbeitenBirthdayDeleted').val(String(eintragBearbeiten.birthdayDeleted)).change();
            $('#bearbeitenButton').attr("data-editid", editId);
        }
    });

    var modal = $(this);
    modal.find('.modal-title').text('Eintrag Nr. ' + editId + ' bearbeiten ' );
});