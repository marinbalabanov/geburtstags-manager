var express = require( 'express' );
var bp = require( 'body-parser' );
var fs = require( 'fs' );
var app = express();

app.use( express.static( 'static' ) );
app.use( bp.urlencoded({ extended:false}) );

function error(status, msg) {
  var err = new Error(msg);
  err.status = status;
  return err;
}

/* Neuen Geburstag hinzufügen */
app.post( '/birthdays/add', function(req, res ) {

  fs.readFile('data/birthdays.json', 'utf8', function(err, contents) {

    var gespeicherteGeburtstage = JSON.parse(contents);
    var tempID = String(gespeicherteGeburtstage.birthdays.length);
    var tempGeburtstagsObj = {id:tempID, birthdayName:req.body.birthdayName, birthdayDay:req.body.birthdayDay, birthdayMonth:req.body.birthdayMonth, birthdayYear:req.body.birthdayYear,birthdayDeleted:"false"};

    gespeicherteGeburtstage.birthdays.push(tempGeburtstagsObj);

    fs.writeFile( 'data/birthdays.json', JSON.stringify( {birthdays:gespeicherteGeburtstage.birthdays} ), function(err) {
      res.status(200).end('OK');
    });
  });
  
});

app.delete( '/birthdays/delete', function(req, res ) {

  fs.readFile('data/birthdays.json', 'utf8', function(err, contents) {
    var geburtstage = {};
    var tempId = req.body.deleteid;
    geburtstage = JSON.parse(contents);
    geburtstage.birthdays[tempId].birthdayDeleted = "true";
    fs.writeFile( 'data/birthdays.json', JSON.stringify( {birthdays:geburtstage.birthdays} ), function(err) {
      res.status(200).end('OK');
    });
  });

});

/* Alle Geburtstage einlesen und ALLE an die UI zurückgeben */
app.get('/birthdays/all', function(req, res){

  fs.readFile('data/birthdays.json', 'utf8', function(err, contents) {
    var geburtstage = {};
    geburtstage = contents;
    res.send(geburtstage);
  });

});

/* Nur EINEN bestimmten Geburtstag an die UI zurückgeben */
app.post('/birthdays/id', function(req, res){

  fs.readFile('data/birthdays.json', 'utf8', function(err, contents) {
    var geburtstage = {};
    var tempId = req.body.singleid;
    geburtstage = JSON.parse(contents);
    var gefundenerGeburtstag = geburtstage.birthdays.find(gefundenerGeburtstag => gefundenerGeburtstag.id === tempId);
    res.send(gefundenerGeburtstag);
  });

});

/* Die Bearbeitung eines Geburtstagseintrags speichern */
app.put('/birthdays/edit', function(req, res){

  fs.readFile('data/birthdays.json', 'utf8', function(err, contents) {
    var geburtstage = {};
    var tempId = parseInt(req.body.editid);

    geburtstage = JSON.parse(contents);
    geburtstage.birthdays[tempId].birthdayName = req.body.editBirthdayName;
    geburtstage.birthdays[tempId].birthdayDay = req.body.editBirthdayDay;
    geburtstage.birthdays[tempId].birthdayMonth = req.body.editBirthdayMonth;
    geburtstage.birthdays[tempId].birthdayYear = req.body.editBirthdayYear;
    geburtstage.birthdays[tempId].birthdayDeleted = req.body.editBirthdayDeleted;
    fs.writeFile( 'data/birthdays.json', JSON.stringify( {birthdays:geburtstage.birthdays} ), function(err) {
      res.status(200).end('OK');
    });

  });

});

/* Fehlerbehandlung */
app.use(function(err, req, res, next){
  res.status(err.status || 500);
  res.send({ error: err.message });
});

app.use(function(req, res){
  res.status(404);
  res.send({ error: '404 error' });
});

/* Server-Port */
app.listen(5003);
console.log('Express started on port 5003');