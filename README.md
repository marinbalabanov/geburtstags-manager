# Geburtstags-Manager

*Achtung: Wie es sich herausstellt, ist diese Lösung nicht RESTFUL, weil nach Schnittstellen-URL unterschieden wird, und nicht nach der Request-Methode.*

Dies ist eine einfache mit NodeJS entwickelte Schnittstelle für das Management von Geburtstagen als Übungsaufgabe für den JavaScript Web Developer-Kurs beim WIFI Wien.

Der Server läuft als Localhost auf Port 5003. Es werden die NodeJS module express, fs und body-parser benötigt. Die Daten werden in der JSON-Datei data/birthdays.json gespeichert.

## User Interface
Im Verzeichnis 'static' befindet sich das ausgearbeitete User Interface für die Verwaltung von Geburtstagen. Es verwendet die Schnittstellen auf http://localhost:5003/birthdays

## Schnittstellenbeschreibung

Folgende Schnittstellen werden bereitgestellt:

### /birthdays/all

Liefert eine vollständige Liste der gespeicherten Geburtstage inklusive Namen, Geburtstag (Tag, Monat, Jahr getrennt voneinander) sowie den Löschstatus.

_Methode:_ GET

_Response:_ Text

### /birthdays/add

Fügt einen neuen Eintrag mit Namen und Geburtsdatum zur Datei hinzu. Beim Anlegen des neuen Eintrags ist der Löschstatus immer "false".

_Methode:_ POST

_Daten:_ birthdayName, birthdayDay, birthdayMonth, birthdayYear

### /birthdays/delete

Markiert einen Eintrag in der Datei als gelöscht. Es werden keine Datensätze aus der Datei tatsächlich gelöscht, um die Folgerichtigkeit der IDs aufrechtzuerhalten.

_Methode:_ DELETE

_Response:_ JSON

_Daten:_ deleteid


### /birthdays/id

Liefert einen Eintrag inklusive Namen, Geburtstag (Tag, Monat, Jahr getrennt voneinander) sowie den Löschstatus anhand der geschickten ID aus der Datei mit den gespeicherten Geburtstage.

_Methode:_ POST

_Response:_ JSON

_Daten:_ singleid

### /birthdays/edit

Ändert den Eintrag in der Datei mit den gespeicherten Geburtstage anhand der geschickten ID.

_Methode:_ POST

_Daten:_ editid, editBirthdayName, editBirthdayDay, editBirthdayMonth, editBirthdayYear, editBirthdayDeleted
